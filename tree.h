#include <queue>
#include <stack>
#include <vector>

using namespace std;

#ifndef Binary_Search_Tree
#define Binary_Search_Tree

template<class T> class Tree;

template<class T>
class Node {
public:
	Node() { left = right = NULL; }
	Node(const T& el, Node *l = 0, Node *r = 0) {
		key = el; left = l; right = r;
	}
	T key;
	Node *left, *right;
};

template<class T>
class Tree {
public:
	Tree() { root = 0; }
	~Tree() { clear(); }
	void clear() { clear(root); root = 0; }
	bool isEmpty() { return root == 0; }
	void inorder() { inorder(root); }
	void insert(const T& el);
	void deleteNode(Node<T> *& node);
	void visit(Node<T> *p);
	void balance(vector<T> data, int first, int last);
	int maxDepth() { return maxDepth(root); }  // function that return max depth

protected:
	Node<T> *root;
	int maxDepth(Node<T> *p);
	void clear(Node<T> *p);
	void inorder(Node<T> *p);

};

template<class T>
void Tree<T>::clear(Node<T> *p)
{
	if (p != 0) {
		clear(p->left);
		clear(p->right);
		delete p;
	}
}


template<class T>
void Tree<T>::inorder(Node<T> *p) {
	if (p != 0) {
		inorder(p->left);
		visit(p);
		inorder(p->right);
	}
}

template<class T>
int Tree<T>::maxDepth(Node<T> *p)
{
	if (p == NULL)
	{
		return 0;//no depth
	}
	else
	{
		int lDepth = maxDepth(p->left);	 //find left depth
		int rDepth = maxDepth(p->right);  //find right depth

		if (lDepth > rDepth) // if left depth more than right depth
			return(lDepth + 1); // return left depth (+1 because the first is 0)  
		else return(rDepth + 1);  // if right depth more than left depth , return right depth
	}
}

template<class T>
void Tree<T>::balance(vector<T> data, int first, int last) {
	if (first <= last) {
		int middle = (first + last) / 2;
		insert(data[middle]);  //insert middle 
		balance(data, first, middle - 1); //call balance from less side (left)
		balance(data, middle + 1, last);  //call balance from more side (right)
	}
}

template<class T>
void Tree<T>::visit(Node<T> *p)
{
	cout << p->key << endl; // print data in node
}


template<class T>
void Tree<T>::insert(const T &el) {
	Node<T> *p = root, *prev = 0;
	while (p != 0) {
		prev = p;
		if (p->key < el)
			p = p->right;
		else
			p = p->left;
	}
	if (root == 0)
		root = new Node<T>(el);
	else if (prev->key < el)
		prev->right = new Node<T>(el);
	else
		prev->left = new Node<T>(el);
}

template<class T>
void Tree<T>::deleteNode(Node<T> *&node) {
	Node<T> *prev, *tmp = node;
	if (node->right == 0)
		node = node->left;
	else if (node->left == 0)
		node = node->right;
	else {
		tmp = node->left;
		prev = node;
		while (tmp->right != 0) {
			prev = tmp;
			tmp = tmp->right;
		}
		node->key = tmp->key;
		if (prev == node)
			prev->left = tmp->left;
		else prev->right = tmp->left;
	}
	delete tmp;
}

#endif // Binary_Search_Tree