#include <iostream>
#include <string>
#include "tree.h"
#include <cstdlib>
#include <ctime>
#include <vector>

using namespace std;

void main()
{
	srand(time(0));
	Tree<int> mytree;
	vector<int> data;

	for (int i = 0; i < 500; i++) {
		int insert = rand();
		mytree.insert(insert);
	}
	mytree.balance(data, 0, data.size() - 1);
	mytree.inorder();
	cout <<endl <<endl <<"The deepest tree is : " <<mytree.maxDepth();



	system("pause");
}